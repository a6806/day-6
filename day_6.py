import argparse
from typing import TextIO
import math


def naive_simulate_day(fishes):
    new_fishes = []
    for fish_index, fish in enumerate(fishes):
        if fish == 0:
            fishes[fish_index] = 6
            new_fishes.append(8)
        else:
            fishes[fish_index] -= 1
    fishes.extend(new_fishes)
    return fishes


offspring_cache = {}


def compute_offspring_count(fish, days):
    # Memoize
    if (fish, days) in offspring_cache:
        return offspring_cache[(fish, days)]

    total_offspring = 0
    original_days = days
    if fish + 1 > days:
        return 0
    else:
        total_offspring += 1
    days -= fish + 1
    total_offspring += math.floor(days / 7)
    total_offspring += compute_offspring_count(8, days)
    days -= 7
    while days > 0:
        total_offspring += compute_offspring_count(8, days)
        days -= 7
    offspring_cache[(fish, original_days)] = total_offspring
    return total_offspring


def part_1(input_file: TextIO):
    fishes = [int(x) for x in input_file.read().split(',')]
    for day_index in range(80):
        fishes = naive_simulate_day(fishes)

    return len(fishes)


def part_2(input_file: TextIO):
    fishes = [int(x) for x in input_file.read().split(',')]
    total = 0
    for fish in fishes:
        total += 1
        total += compute_offspring_count(fish, 256)
    return total


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 6')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
